# test-nsc
1. Clone the repository: `git clone https://taylorwebk@bitbucket.org/taylorwebk/test-nsc.git`
2. Go to folder: `cd test-nsc`
3. Install the dependences: `yarn install` or `npm install`
4. Open your browser and go to: `localhost:8080`. The backend is running in `localhost:3000`

## Folder structure
```
._api              // the backend code
|  |_database      // the database logic (CRUD)
|_src              // the front-end code
   |_Components    // Single Components
   |_Screens       // Screens of App
   |_utils         // Some util constants
```

### 1. Where can I deploy this solution?
If this will be a middle-small project, the best platform to deploy I think would be heroku, because this platform was bord as one of the first solutions for node js applications.
Otherwise, Amazon Web Services has a solid architecture.
### 2. How can I support multiple countries?
Supporting several countries implies supporting several languages and currencies, this can be solved by adding fields to handle the type of currency and a dictionary of languages in the front end for languages.
### 3. How can I support stress times on day like Black Friday?
An good solution could be to store the data in dedicated servers by region and cache some data in the server.
### 4. Please help me on this and consider funding will be limited at the beginning
If there is no money to pay me, I want a 10% of shares. :P