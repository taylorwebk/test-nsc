import React, { useState, useEffect } from 'react'
import axios from 'axios'
import Typography from '@material-ui/core/Typography'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'

import Container from '../Components/Container'
import NewLoan from '../Components/NewLoan'
import Info from '../Components/Info'
import endpoints from '../utils/constants'

const Home = () => {
  const [email, setEmail] = useState('')
  const [errMsg, setErrMsg] = useState('')
  const [emailObj, setEmailObj] = useState({})
  const [withAccount, setWithAccount] = useState(null)

  const handleEmailInput = (e) => {
    const { value } = e.target
    setWithAccount(null)
    setEmail(value)
  }
  const handleCheck = () => {
    if (!/^[^@\s]+@[^@\s]+\.[^@\s]+$/.test(email)) {
      setErrMsg('Please enter a valid email')
      return
    }
    setErrMsg('')
    axios.post(endpoints.info, { email })
      .then(({ data }) => {
        if (data === '') {
          setWithAccount(false)
        } else {
          setEmailObj(data)
          setWithAccount(true)
        }
      })
  }
  const updateInfo = () => axios.post(endpoints.info, { email })
    .then(({ data }) => {
      if (data !== '') {
        setEmailObj(data)
        setWithAccount(true)
      }
    })
  useEffect(() => {
  }, [])
  return (
    <Container>
      <Typography color="primary" variant="h6">
        Loans and Payments
      </Typography>
      <br />
      <TextField
        value={email}
        placeholder="Enter email"
        onChange={handleEmailInput}
        error={errMsg.length > 0}
        helperText={errMsg}
      />
      <br />
      <Button variant="contained" color="primary" onClick={handleCheck} disabled={typeof withAccount === 'boolean'}>
        Check
      </Button>
      <br />
      { withAccount === false && <NewLoan email={email} updateInfo={updateInfo} /> }
      { withAccount === true && <Info emailObj={emailObj} email={email} updateInfo={updateInfo} /> }
    </Container>
  )
}

export default Home
