const baseUrl = 'http://localhost:3000/'

const endpoints = {
  loan: `${baseUrl}loan`,
  payments: `${baseUrl}payments`,
  info: `${baseUrl}information`
}

export default endpoints
