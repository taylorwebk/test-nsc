import React, { useState, Fragment } from 'react'
import axios from 'axios'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'

import endpoints from '../utils/constants'

const NewLoan = ({ email, updateInfo }) => {
  const [input, setInput] = useState('')
  const [errMsg, setErrMsg] = useState('')
  const handleInput = (e) => {
    const { value } = e.target
    setInput(value)
  }
  const handlePost = () => {
    if (!/^[1-9][0-9]*$/.test(input)) {
      setErrMsg('Please enter a valid integer number greather than 0')
      return
    }
    setErrMsg('')
    const amount = Number.parseInt(input, 10)
    axios.post(endpoints.loan, { email, amount })
      .then(({ data }) => {
        if (Object.keys(data).length === 0) {
          updateInfo()
        } else {
          setErrMsg(data.msg)
        }
      })
  }
  return (
    <Fragment>
      <Typography color="textPrimary">
        {`You actually has no debts, you can create one by making a loan for: ${email} below`}
      </Typography>
      <TextField
        error={errMsg.length > 0}
        helperText={errMsg}
        value={input}
        onChange={handleInput}
        placeholder="Enter amount here"
        inputProps={{
          type: 'number'
        }}
      />
      <br />
      <Button onClick={handlePost} color="primary" variant="contained">
        Create account and loan
      </Button>
    </Fragment>
  )
}
export default NewLoan
