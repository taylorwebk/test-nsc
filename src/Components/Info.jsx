import React, { useState, Fragment } from 'react'
import axios from 'axios'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListSubheader from '@material-ui/core/ListSubheader'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'

import endpoints from '../utils/constants'

const Info = ({ email, emailObj, updateInfo }) => {
  const [input, setInput] = useState('')
  const [errMsg, setErrMsg] = useState('')
  const { loans, payments, amount } = emailObj
  const handleInput = (e) => {
    const { value } = e.target
    setInput(value)
  }
  const handleSubmit = key => () => {
    if (!/^[1-9][0-9]*$/.test(input)) {
      setErrMsg('Please enter a valid integer number greather than 0')
      return
    }
    setErrMsg('')
    const inputAmount = Number.parseInt(input, 10)
    axios.post(endpoints[key], { email, amount: inputAmount })
      .then(({ data }) => {
        if (Object.keys(data).length === 0) {
          updateInfo()
          setInput('')
        } else {
          setErrMsg(data.msg)
        }
      })
  }
  return (
    <Fragment>
      <Typography variant="h6">
        My loans and payments
      </Typography>
      <Grid container direction="row" justify="center">
        <Grid item>
          <List
            subheader={<ListSubheader>Loans</ListSubheader>}
          >
            {
              loans.map((loan, i) => (
                <ListItem key={i}> {/* eslint-disable-line */}
                  <Typography>{loan}</Typography>
                </ListItem>
              ))
            }
          </List>
        </Grid>
        <Grid item>
          <List
            subheader={<ListSubheader>Payments</ListSubheader>}
          >
            {
              payments.map((payment, i) => (
                <ListItem key={i}> {/* eslint-disable-line */}
                  <Typography>{payment}</Typography>
                </ListItem>
              ))
            }
          </List>
        </Grid>
      </Grid>
      <Typography variant="h5" color="secondary">
        {`You current balance is: ${amount}`}
      </Typography>
      <br />
      <TextField
        error={errMsg.length > 0}
        helperText={errMsg}
        value={input}
        onChange={handleInput}
        placeholder="Insert here the price"
      />
      <br />
      <Grid container direction="row" justify="center">
        <Grid item>
          <Button color="secondary" variant="contained" onClick={handleSubmit('loan')}>
            New Loan
          </Button>
        </Grid>
        <Grid item>
          <Button color="primary" variant="contained" onClick={handleSubmit('payments')}>
            New Payment
          </Button>
        </Grid>
      </Grid>
    </Fragment>
  )
}

export default Info
