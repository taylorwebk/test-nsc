import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Paper from '@material-ui/core/Paper'

const styles = makeStyles(() => ({
  cont: {
    maxWidth: 800,
    flexGrow: 1
  },
  divCont: {
    display: 'flex',
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center'
  },
  paper: {
    width: '100%',
    marginTop: 25,
    padding: 12,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column'
  }
}))

const Container = ({ children }) => {
  const classes = styles()
  return (
    <div className={classes.divCont}>
      <Grid container className={classes.cont}>
        <Paper className={classes.paper}>
          { children }
        </Paper>
      </Grid>
    </div>
  )
}
export default Container
