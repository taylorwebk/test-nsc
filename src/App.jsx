import React from 'react'
import {
  BrowserRouter as Router, Route
} from 'react-router-dom'
import withTheme from './utils/withTheme'

import Home from './Screens/Home'

const App = () => (
  <Router>
    <Route path="/" component={Home} exact />
  </Router>
)

export default withTheme(App)
