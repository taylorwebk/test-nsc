
/**
 * SAMPLE db OBJ
 * {
 *   "user@gmail.com": {
 *     loans: [100, 399, 445],
 *     payments: [299, 4344]
 *   },
 *   "user2@gmail.com": {
 *     loans: [],
 *     payments: []
 *   }
 * }
 */

module.exports = (db = {}) => {
  const existEmail = (email) => {
    if (typeof db[email] === 'undefined') {
      return false
    }
    return true
  }
  const createEmail = (email) => {
    db[email] = {}
    db[email].loans = []
    db[email].payments = []
  }
  const getObj = email => db[email]
  const newLoan = (email, cant) => {
    db[email].loans = [...db[email].loans, cant]
  }
  const newPayment = (email, cant) => {
    db[email].payments = [...db[email].payments, cant]
  }
  const getBalance = (email) => {
    const { loans, payments } = db[email]
    return loans.reduce((sum, val) => sum + val, 0) - payments.reduce((sum, val) => sum + val, 0)
  }
  const getLoans = email => db[email].loans
  const getPayments = email => db[email].payments
  return {
    existEmail,
    createEmail,
    newLoan,
    newPayment,
    getBalance,
    getLoans,
    getPayments,
    getObj
  }
}
