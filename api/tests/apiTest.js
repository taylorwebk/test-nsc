const request = require('supertest')
const app = require('../main')

// Testing default endpoint: HELLO WORLD
describe('GET /', () => {
  it('Respond with Hello world.', (done) => {
    request(app)
      .get('/')
      .expect(200, done)
  })
})
describe('POST /loan', () => {
  const data = {
    email: 'new@new.com',
    amount: 300
  }
  const data2 = {
    email: 'new@new.com',
    amount: 50
  }
  it('Respond with error if is a new email and loan exceeds 50', (done) => {
    request(app)
      .post('/loan')
      .send(data)
      .expect('Content-Type', /json/)
      .expect({ code: 100, msg: 'Amount exceeded' })
      .end((err) => {
        if (err) {
          done(err)
          return
        }
        done()
      })
  })
  it('Respond with empty object if is a new email and loan is eq or below 50', (done) => {
    request(app)
      .post('/loan')
      .send(data2)
      .expect('Content-Type', /json/)
      .expect({})
      .end((err) => {
        if (err) {
          done(err)
          return
        }
        done()
      })
  })
})
describe('POST /payments', () => {
  const data = {
    email: 'new2@new.com',
    amount: 500
  }
  const data2 = {
    email: 'new@new.com',
    amount: 30
  }
  it('Return error 100 when uses does not exist.', (done) => {
    request(app)
      .post('/payments')
      .send(data)
      .expect('Content-Type', /json/)
      .expect({ code: 100, msg: 'Email not exists' })
      .end((err) => {
        if (err) {
          done(err)
          return
        }
        done()
      })
  })
  it('Return empty object when operatios was successfully.', (done) => {
    request(app)
      .post('/payments')
      .send(data2)
      .expect('Content-Type', /json/)
      .expect({})
      .end((err) => {
        if (err) {
          done(err)
          return
        }
        done()
      })
  })
  it('Return error 100 when the amount exceeds the debt.', (done) => {
    request(app)
      .post('/payments')
      .send(data2)
      .expect('Content-Type', /json/)
      .expect({ code: 100, msg: 'Amount exceeds debt' })
      .end((err) => {
        if (err) {
          done(err)
          return
        }
        done()
      })
  })
})
describe('GET /information', () => {
  it('Return a object with email as key and two arrays with one data', (done) => {
    request(app)
      .post('/information')
      .send({ email: 'new@new.com' })
      .expect('Content-Type', /json/)
      .expect({ amount: 20, loans: [50], payments: [30] })
      .end((err) => {
        if (err) {
          done(err)
          return
        }
        done()
      })
  })
  it('Return a empty response is emails does not exist', (done) => {
    request(app)
      .post('/information')
      .send({ email: 'new@newnew.com' })
      .expect('Content-Type', /json/)
      .expect('')
      .end((err) => {
        if (err) {
          done(err)
          return
        }
        done()
      })
  })
})
