const express = require('express')

const app = express()

const staticDb = require('./database/index')

app.use(express.json())
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
  next()
})

const db = staticDb()

const MAX_AMOUNT = 1000

const responses = {
  badRequest: {
    msg: 'Error desde el front'
  },
  error: (code, msg) => ({
    code,
    msg
  }),
  okWithData: data => ({
    data
  }),
  ok: {}
}

app.get('/', (req, res) => {
  res.send('Hello World!')
})
app.post('/loan', (req, res) => {
  const { email, amount } = req.body
  if (!email || !amount) {
    res.status(400)
    res.json(responses.badRequest)
    return
  }
  if (!db.existEmail(email)) {
    if (amount > 50) {
      res.json(responses.error(100, 'Amount exceeded'))
      return
    }
    db.createEmail(email)
    db.newLoan(email, amount)
    res.json(responses.ok)
    return
  }
  const balance = db.getBalance(email)
  if (balance + amount > MAX_AMOUNT) {
    res.json(responses.error(100, 'Amount exceeded'))
    return
  }
  db.newLoan(email, amount)
  res.json(responses.ok)
})
app.post('/payments', (req, res) => {
  const { email, amount } = req.body
  if (!email || !amount) {
    res.status(400)
    res.json(responses.badRequest)
    return
  }
  if (!db.existEmail(email)) {
    res.json(responses.error(100, 'Email not exists'))
    return
  }
  const balance = db.getBalance(email)
  if (balance - amount < 0) {
    res.json(responses.error(100, 'Amount exceeds debt'))
    return
  }
  db.newPayment(email, amount)
  res.json(responses.ok)
})
app.post('/information', (req, res) => {
  const { email } = req.body
  if (typeof email === 'undefined') {
    res.status(400)
    res.json(responses.badRequest)
    return
  }
  if (!db.existEmail(email)) {
    res.json()
    return
  }
  const data = {
    amount: db.getBalance(email),
    loans: db.getLoans(email),
    payments: db.getPayments(email)
  }
  res.json(data)
})

app.listen(3000, () => {
  console.log('SERVER IS RUNNING ON PORT 3000')
})

module.exports = app
